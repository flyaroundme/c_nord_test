import time
from tornado.tcpserver import TCPServer
from tornado.iostream import StreamClosedError, IOStream
from tornado.ioloop import IOLoop
from tornado import gen, queues
from messages import SourceMessage, ServerMessage, MessageParsingError, ServerStatus, SourceStatus


class SourceServer(TCPServer):

    def __init__(self, mq: queues.Queue):
        super().__init__()
        self.mq = mq

    @gen.coroutine
    def handle_stream(self, stream: IOStream, address):
        while True:
            try:
                if not stream.reading():
                    data = yield stream.read_until(b"\n")
                    msg = yield self._process_message(data)
                    yield stream.write(msg)
            except StreamClosedError:
                break

    @gen.coroutine
    def _process_message(self, data):
        try:
            msg = SourceMessage.parse(data)
            header = ServerStatus.MSG_PARSE_OK
            msg_num = msg.data["msg_num"]

            yield self.mq.put(msg.data)

        except MessageParsingError:
            header = ServerStatus.MSG_PARSE_FAIL
            msg_num = 0
        return ServerMessage(header, msg_num).msg


class ClientServer(TCPServer):
    def __init__(self, mq, sources):
        super().__init__()
        self.mq = mq
        self.sources = sources
        self.streams = list()

    @gen.coroutine
    def handle_stream(self, stream, address):
        try:
            self.streams.append(stream)
            msg = "\r\n".join(
                "[%s] %i | %s | %s" % (
                    k, v["last_msg_num"], SourceStatus(v["status"]).name,
                    int((float(time.time()) - v["timestamp"]) * 1000))
                for k, v in self.sources.items()
            )
            yield stream.write(msg.encode())

        except StreamClosedError:
            pass


class App:
    def __init__(self):
        self.sources_mq = queues.Queue()
        self.clients_mq = queues.Queue()
        self.sources = dict()
        self.source_server = SourceServer(self.sources_mq)
        self.client_server = ClientServer(self.clients_mq, self.sources)

    @gen.coroutine
    def sources_mq_callback(self):
        while True:
            try:
                data = yield self.sources_mq.get()
                self.sources.update({
                    data["source_id"]: {
                        "last_msg_num": data["msg_num"],
                        "status": data["source_status"],
                        "timestamp": float(time.time())
                    }
                })
                client_msg = "\r\n".join(
                    "[%s] %s | %i" % (data["source_id"], field["name"], field["value"])
                    for field in data["fields"]
                )
                yield self.clients_mq.put(client_msg)
            except queues.QueueEmpty:
                pass

    @gen.coroutine
    def clients_mq_callback(self):
        while True:
            try:
                data = yield self.clients_mq.get()

                for stream in self.client_server.streams:
                    try:
                        if not stream.writing():
                            yield stream.write(data.encode())
                    except StreamClosedError:
                        pass
            except queues.QueueEmpty:
                pass

    def start(self):
        self.source_server.listen(8888, "0.0.0.0")
        self.client_server.listen(8889, "0.0.0.0")
        loop = IOLoop.instance()
        loop.add_future(self.sources_mq_callback(), lambda f: f.result())
        loop.add_future(self.clients_mq_callback(), lambda f: f.result())
        loop.start()


if __name__ == "__main__":
    App().start()
