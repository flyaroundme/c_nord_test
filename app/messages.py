from construct import Const, Int16ub, PaddedString, Int32ub, Struct, Byte, Array, this, \
    ConstructError
from functools import reduce

from enum import Enum, unique


class MessageParsingError(Exception):
    pass


@unique
class SourceStatus(Enum):
    IDLE = 0x01
    ACTIVE = 0x02
    RECHARGE = 0x03


class ServerStatus:
    MSG_PARSE_OK = 0x11
    MSG_PARSE_FAIL = 0x12


class BaseMessage:
    FORMAT = Struct()

    def __init__(self, *args, **kwargs):
        self._data = dict()
        self.checksum = b"\x00"

    @property
    def data(self):
        return self._data

    @property
    def msg(self):
        _msg = self.FORMAT.build(self._data)
        self.checksum = self._calc_checksum(_msg)
        return _msg + self._calc_checksum(_msg) + b"\r\n"

    @staticmethod
    def _calc_checksum(msg):
        cs = reduce(lambda x, y: x ^ y, msg, 0)
        return cs.to_bytes(1, byteorder="big")


class SourceMessage(BaseMessage):
    FORMAT = Struct(
        "header" / Const(0x01, Byte),
        "msg_num" / Int16ub,
        "source_id" / PaddedString(8, "ascii"),
        "source_status" / Byte,
        "numfields" / Byte,
        "fields" / Array(this.numfields, Struct(
            "name" / PaddedString(8, "ascii"),
            "value" / Int32ub
        ))
    )

    def __init__(self, msg_num, source_id, source_status, numfields, fields):
        super().__init__()
        self._data = dict(
            msg_num=msg_num,
            source_id=source_id,
            source_status=source_status,
            numfields=numfields,
            fields=fields,
        )

    @classmethod
    def parse(cls, msg):
        try:
            msg = msg.strip()
            p = cls.FORMAT.parse(msg[:-1])
            message_object = cls.__call__(
                msg_num=p.msg_num,
                source_id=p.source_id,
                source_status=p.source_status,
                numfields=p.numfields,
                fields=p.fields
            )
            message_object.checksum = msg[-1]
            return message_object
        except ConstructError as e:
            raise MessageParsingError(e)


class ServerMessage(BaseMessage):
    FORMAT = Struct(
        "header" / Byte,
        "msg_num" / Int16ub
    )

    def __init__(self, header, msg_num):
        super().__init__()
        self._data = dict(
            header=header,
            msg_num=msg_num
        )

    @classmethod
    def parse(cls, msg):
        try:
            msg = msg.strip()
            p = cls.FORMAT.parse(msg[:-1])
            message_object = cls.__call__(
                header=p.header,
                msg_num=p.msg_num
            )
            message_object.checksum = msg[-1]
            return message_object
        except ConstructError as e:
            raise MessageParsingError(e)

    @classmethod
    def sizeof(cls):
        return cls.FORMAT.sizeof() + 3
