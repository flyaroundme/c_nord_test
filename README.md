# Installation & usage
```bash
pip install -r requirements.txt

python app/server.py
```

# Notes
It is considered that each source message is also terminated by `\r\n` due to variability of message length. 

See `simple_source.py` & `simple_client.py`

Tested with Python 3.6.4
