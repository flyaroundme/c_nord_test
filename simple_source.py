import socket
from app.messages import SourceMessage, ServerMessage, SourceStatus

if __name__ == "__main__":
    m1 = SourceMessage(0, "SOURCE1", SourceStatus.ACTIVE.value, 1, [dict(name="hello", value=3)])
    print(m1.msg)
    conn_1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    conn_1.connect(("127.0.0.1", 8888))
    conn_2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    conn_2.connect(("127.0.0.1", 8888))
    m2 = SourceMessage(0, "SOURCE2", SourceStatus.IDLE.value, 1, [dict(name="hey", value=0)])
    print(m2.msg)
    conn_1.send(m1.msg)
    conn_2.send(m2.msg)
    data_1 = conn_1.recv(ServerMessage.sizeof())
    data_2 = conn_2.recv(ServerMessage.sizeof())
    print(ServerMessage.parse(data_1).data)
    print(ServerMessage.parse(data_2).data)
    conn_1.close()
    conn_2.close()
