import socket
import time

if __name__ == "__main__":
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(("127.0.0.1", 8889))
    try:
        while True:
            data = client.recv(4096)
            print(data)
            time.sleep(0.1)
    finally:
        client.close()

